                         THE PYTHON LANGUAGE
                                2.3.5


Dependencies
------------
Although Python is cross-platform, the file that accompanies these 
instructions will only work with Windows. If you have another 
operating system, please see the official Python web site.


Installation
------------
To install Python, simply run the Python set-up program, 
Python-2.3.5.exe, by double-clicking it, and follow the prompts.  
You may accept all default settings.


Testing
-------
From the Start menu, choose Programs, then choose Python 2.3, then 
choose IDLE (Python GUI).  If IDLE does not appear, you may need to 
update your PATH to include the folder in which you installed Python. 


Official Site
-------------
For more information, visit the official Python web site at 
http://www.python.org.
