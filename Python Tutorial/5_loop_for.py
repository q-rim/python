print "\n\nEX1: for loop:  traverse a list ------------------------------------------------------------------------------------"
# Example 'for' loop
# for loops are great for traversing lists, tuples, dictionaries:
# First, create a list to loop through:

newList = ['eat  me', 90210, 'The day has come, the walrus said, to speak of many things', -67, 'test', 45]
print "newList = ", newList

# create loop:
# goes through newList and sequentially puts each bit of information into the variable value 
# and runs the loop
i=0
for n in newList:		
	i+=1; print "i =",i  		# print counter
	print n 					# print items in the list


print "\n\nEX2: for loop:  traverse each letter of a word ------------------------------------------------------------------------------------"
# Example 'for' loop
# cheerleading program
word = raw_input("Who do you go for? ")		# prompt user.  Obtain input

for letter in word:							# for loop on each letter of the word
	call = "Gimme a " + letter + "!"		
	print call
	print letter + "!"

print "What does that spell?"
print word + "!"



print "\n\nEX3: for loop:  Menu Function ------------------------------------------------------------------------------------"
# Example 'for' loop

# Menu Function
def menu(list, question):
	for entry in list:						# just prints out the menu one item at a time.
		print 1 + list.index(entry),		# uses index() to find where in the list the entry is in.  Remember, List index starts with 0
		print ") " + entry

	return input(question)					# get the user input.  Return this value

# def menu(list, question): is telling the function to ask for two info:
# 1. list of all menu entries.
# 2. question it will ask when all the options have been printed.


# Item inspection function
def inspect(choice, location, list):					# 1 - something is found.  0 - nothing is found.
	if choice == location:
		print "You found a key at the: ", list[choice-1];
		print "now find the exit door!"
		return 1
	else:
		print "Nothing of interest at the: ", list[choice-1];		
		return 0

# Basic info about the room:
items = ['pot plant', 'painting', 'vase', 'lampshade', 'shoe', 'door']; 		#print items
keylocation = 3		# The Key is in the vase (or entry number "keylocation" in the list above).

# Game
print "~~~ Welcome to the Text Adventure Game:  What's in the Room? ~~~"

# Give some introductory text:
print "Last night you went to sleep in the comfort of your own home."
print "Now, you find yourself locked in a room. You don't know how you got there, or what time it is."
print "In the room you can see:"
print len(items), "things:"

i=0;
for x in items:
	i+=1; #print "i =",i           		# print counter
	print 'item', i, 'is:  ', x			# print items in the list

print ""
print "The door is locked.  Could there be a key somewhere?"
print "keylocation =", keylocation; 	print;			# this is to help me trouble shoot.

# # Get your program working and program until you find the key.
# loop = 1; 	keyFound = 0; player_choice = 0;	foundExit=0;	keyHasBeenFound=0;
# while keyHasBeenFound==0 or player_choice!=6:
# 	player_choice = menu(items, "What do you want to inspect? "); 			print "player_choice =", player_choice;
# 	keyFound = inspect(player_choice, keylocation, items);					print "keyFound =", keyFound;

# 	if keyFound == 1:
# 		keyHasBeenFound = 1;
# 	print "keyHasBeenFound =", keyHasBeenFound;
# 	print
		

# Get your program working and program until you find the key.
loop = 1; 	keyFound = 0; player_choice = 0;	foundExit=0;
while loop==1:
	player_choice = menu(items, "What do you want to inspect? "); 			print "player_choice =", player_choice;
	keyFound = inspect(player_choice, keylocation, items);					print "keyFound =", keyFound;

	if keyFound == 1:
		print "You've found the key.  Now find the exit!"; 	print;
		while foundExit!=6:
			print "foundExit =", foundExit;
			foundExit = menu(items, "find the door:");	print;
		loop = 0;
	print



	# choice = menu(items, "What do you want to inspect? "); 		print "choice is: ",choice;

	# #if (choice == 1) or (choice == 2) or (choice == 3) or (choice == 4) or (choice == 5):
	# if (1 <= choice <= 5):
	# 	if choice == keylocation:
	# 		print "You found key in the ", items[choice-1];		print ""
	# 		keyfound = 1
	# 	else:
	# 		print "You found nothing in the ", items[choice-1]; 	print ""
		
	# elif choice == 6:
	# 	if keyfound == 1:
	# 		loop = 0
	# 		print "You put in the key, turn it, and hear a click.";			print ""
	# 		keyfound = 1
	# 	else:
	# 		print "The door is locked.  You must find the key first"; 		print ""	



print "\nLight floods into room as you open the door to your freedom!!!"; 	print "~~~ Game Over ~~~";	print;
