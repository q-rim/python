print "\n\n----------------- Tuples -----------------"
# (Tuples) - similar to lists, but values cannot be changed. ##########################################
#   - You cannot add or remove
#   ex:  months of the year

# months = 'Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Sep', 'Oct', 'Nov', 'Dec'		#  Parenthesis is not needed, but use it.
# months = ('Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', \
# 	'Jul', 'Sep', 'Oct', 'Nov', 'Dec')														# you can spill ove to the next line by using '/'
months = ('Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec')

print months
print months[0]						# value of month at index 0
print len(months)					# length of month
myMonths = months[3], months[0]		# create Tuples with these values
print myMonths



print "\n\n----------------- Lists -----------------"
# [Lists] - list of values. ##########################################
#   - You can remove values from the list.
#   - You can add values to the end.
#   ex:  names of your classmates.

cats = ['Tom', 'Snappy', 'Kitty', 'Jessie', 'Chester']
print cats
print cats[1]						# value of list at index 1
cats.append('Catherine')			# add value to the end
cats.append(234)					# append an interger to the end
print cats
del cats[1]							# delete a value from list
print cats
cats.sort()							# sort list
print cats 							
print len(cats)						# length of list



print "\n\n----------------- Dictionaries -----------------"
# {Dictionaries} = {key:value} ##########################################
#   - each key is called words as in words of a dictionary.
#   - ordering is done by hash table.
#   ex:  telephone book.  Has name:number = key:value

phonebook = {								# create dictionary
	'qq':12345, 
	'Yongrim':236, 
	'Juju':12343, 
	'Elisa Rhee':34673,
	'Joker':294384,
}
print phonebook

phonebook['Gingerbread Man'] = 1234567		# add to dictionary
print phonebook

del phonebook['Joker']						# deleting from dictionary
print phonebook

print phonebook.has_key('Juju')				# has_key('Juju') function - does a key exist?  Returns True or False
print phonebook['Juju'] 					# value to the keys
print phonebook.keys()						# create a list of keys
print phonebook.values()					# create a list of values
print len(phonebook)						# length of dictionary

print("test")

