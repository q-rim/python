# calculator program:
# 1. prompt the user arithmatic.
def printMenu():							# Create Function with no input argument
	print ""								# function is enclosed by Tab, not parenthesis
	print "Calculator:"
	print "1) + addition"
	print "2) - subtraction"
	print "3) x multiplilcation"
	print "4) / division"
	print "5) quit"
	print ""
	return input("Enter your option: ")		# return value.  End of Function.


choice = printMenu()						# Using a Function.
x1 = input("first number: ")				# Using a Function that takes in a variable.
x2 = input("second number: ")

# 2. Create arithmatic functions.
def add(a,b):								# Create Function with input argument
	ans = a+b
	return ans

def sub(a,b):
	ans = a-b
	return ans

def mult(a,b):
	ans = a*b
	return ans

def div(a,b):
	ans = a/b
	return ans


# # switch case statement
# options = {1 : add,
# 			2 : sub,
# 			3 : mult,
# 			4 : div,
# 			5 : add,
# }

# options[choice]()

if choice==1:
	ans = add(x1, x2)

elif choice==2:
	ans = sub(x1, x2)

elif choice==3:
	ans = mult(x1, x2)

elif choice==4:
	ans = div(x1, x2)

elif choice==5:
	print "quit() calculator"
	quit()

else:
	print "bad input"
	print "quit() calculator"
	quit()


# 3. Output the solution.
print "ans=", ans