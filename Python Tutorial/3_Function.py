#############################################################################
# EX1:  function with no input.
print "---------- function with no input:  hello() ----------"
def hello():				# define a function
	print "hello"
	return 1234

print hello()
a= hello()
print a
print "\n"


#############################################################################
# EX2:  pre-defined functions:
print "---------- input(\"prompt\") ----------"
# input("prompt") 
input1 = input("enter numeric input: ")
print input1
print "\n"



#############################################################################
# EX3:  function with input parameters
print "---------- function with input parameters ----------"
# add(x1, x2)
def add(a,b):		# define the function
		c = a + b
		return c


x1 = 5
x2 = 2
x3 = add(x1, x2)	# using the function
print "x3 = add(x1, x3)"
print "x3 =", x3

