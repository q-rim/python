# Numerical Variables demonstrated

# print function takes string, int, double
print 
print "Demo of Numerical Variables"
print "var \tanswer"
v=1
print "v=1; \tans:", v
v=1+1
print "v=v+1; \tans:", v
v=51.55
print "v=51, \tans:", v

print 
print 

# String Variables demonstrated

print "Demo of String Variables"
word1 = "Good"
word2 = "Morning"
word3 = "to you!"

print word1
# you can print multiple string.  It will add a string
print word1, word2
# Concatinate string
sentence =  word1 +" "+ word2 +" "+ word3
print sentence