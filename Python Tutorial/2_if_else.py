# if elif demonstration

# Test Variables:
#y = -1
#y = 10
y = -5.55
y = "test"			# for some reason, python treats string as positive int or double?

if y==0:
	print "y==0"
elif y<0:
	print "y is negative"
elif y>0:
	print "y is positive"
else:
	print "bad input"


print "y =", y