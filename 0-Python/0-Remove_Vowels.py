#! /usr/bin/python

userIN = "This is a TEST, remove all vowels"
vowel = "aeiouAEIOU"


# NOTE 
# Depending on the TYPE you use on for-loops, it will iterate through that type.
# EX:
#if you use for x in <TYPE> where TYPE is a string, then x will iterate through each char of the stirng.


noVowel = ""
for char in userIN:
	if char not in vowel:
		noVowel = noVowel+char
print userIN
print noVowel


noVowel = ""
for i in range(len(userIN)):
	if userIN[i] not in vowel:
		noVowel = noVowel+userIN[i]
print userIN
print noVowel