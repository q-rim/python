#! /usr/bin/python


# NOTE:  private is denoted with leading "__":
# __private_attribute
# __private_method(self):


# Object Encapsulation - Object's privacy.  Got to respect Object's privacy!

# private Attributes:  self.__private_attribute
#     should be accessed by class method.  They shouldn't be accessed directly.  
#	  However, you can still access it:  crit._Critter__mood

# private Methods:  self.__private_method()
#     should also be accessed by class method.
#	  However, you can still access it:  crit._Critter)__private()


class Critter(object):

	def __init__(self, name, mood):
		self.name = name
		self.__mood = mood
		print "New critter: ", name, mood


	def talk(self):
		print self.name
		print self.__mood

	def __private_method(self):
		print "private_method()"

	def public_method(self):
		self.__private_method()
		print "public_method()"

crit1 = Critter("Kyurim", "angry!")
#crit1.public_method()
crit1._Critter__private_method()

crit1.public_method()

crit1.talk()