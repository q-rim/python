#! /usr/bin/python
import sys;

def isPowerOf2(uIN):
	i=0
	while 2**i <= uIN:
		#print i, 2**i
		if 2**i == uIN:
			return True
		i +=1
	return False


# main
# check to see if user prompt is neede or run direct from CLI.
if len(sys.argv) <= 1:
	argX = input("enter a test value:  ")
else:
	argX = int(sys.argv[1])


print isPowerOf2(argX)

