#! /usr/bin/python


# #### Get line
# txt = open("0.txt", "r")
# for line in txt:
# 	print line
# txt.close()


# #### Get words
# txt = open("0.txt", "r")
# for line in txt:
# 	for word in line.split():
# 	 	print word
# txt.close()


# #### Get charactors
# txt = open("0.txt", "r")
# for line in txt:
# 	for char in line:
# 	 	print char
# txt.close()

keyword = "Kyurim"

# #### find line#, word.postion
# def getLineWordPosition(keyword):
# 	# returns first instance of the word
# 	# returns only the exact match
# 	txt = open("0.txt", "r")
# 	count_line = 0
# 	for line in txt:
# 		if keyword in line:
# 			linePOS = count_line
# 		count_line +=1

# 		count_word = 0
# 		for word in line.split():
# 			if word == keyword:
# 				wordPOS = count_word
# 				return [linePOS, wordPOS]
# 			count_word +=1

# print getLineWordPosition(keyword)



# #### find line#, string.postion
# def getLineStrPosition(keyword):
# 	# returns first instance of the str
# 	# returns only  the exact match
# 	txt = open("0.txt", "r")
# 	count_line = 0
# 	for line in txt:
# 		if keyword in line:
# 			linePOS = count_line
# 		count_line +=1

# 		count_word = 0
# 		for word in line.split():		# takes line and creates a list of words
# 			print "word[" +str(count_word)+ "]: " ,word
# 			if keyword in word:
# 				wordPOS = count_word
# 				return [linePOS, wordPOS]
# 			count_word +=1

# print getLineStrPosition(keyword)


txt = open("0.txt", "r")
for line in txt:
	# print line
	words = line.split()	# split(): puts each word into a list
	print words
	# print words[4]



# find the 4th octect on IP.
IP = "192.168.25.50"
octet = IP.split(".")		# split(delimeter):  puts each segment into a list
print octet
print "4th octet:", octet[3]


