#!/usr/bin/env python 
# http://ilab.cs.byu.edu/python/socket/echoserver.html

#A simple echo server 

import socket 

s = socket.socket(socket.AF_INET, socket.SOCK_STREAM) 
host = "" #socket.gethostname()
port = 50000 
backlog = 5 
size = 1024 
print "Sever Listening:\nhostname:\t", host, "\nport:\t", port, "\nbacklog:\t", backlog, "\nsize(Packet?):\t"

s.bind((host,port)) 
s.listen(backlog) 
while True: 
    client, address = s.accept() 
    print "Got connection from:", address
    client.send("Server: Connection Established")
    data = client.recv(size) 
    if data: 
        client.send(data) 
    client.close()