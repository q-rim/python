#! /usr/bin/python

# this program finds all prime numbers:
n=10
# for i in range(n):
# 	i +=1	
# 	print n-i


# print range(6)



def isPrime(testVal):
	# returns true if testVal is a prime int.
	for i in range(2, testVal):			# [0,i]
		#print testVal, "%", i, "=", testVal%i
		if testVal%i == 0:		# if there is no remainder, then the number is divisible.
			# print "isNotPrime"
			return False		
	return True;


# main goes through the test values to see if they are prime int.
n=100
prime=[]
for i in range(1,n):
	if isPrime(i):
		prime.append(i)
		print "testing", i, isPrime(i), prime


print prime




