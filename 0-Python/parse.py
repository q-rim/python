#! /usr/bin/python
# parse.py parsing practice


# get line.
txt = open("parse.txt" ,'r')
for line in txt:
	print line,						# line is a list of char.
txt.close()


# get a line into list using split():
# txt = open("parse.txt" ,'r')
# lines = string.split("parse.txt", '\n')  		# lines = ['Line1', 'Line2', 'Line3'] in a doc
# txt.close()


# get an array of charactors
# txt = open("parse.txt" ,'r')
# for line in txt:
# 	print line[0:10]				# print char from 1:10
# txt.close()


# get charactor by charactor
# txt = open("parse.txt" ,'r')
# for line in txt:
# 	for i in line:
# 		print i,					# print char by char
# txt.close()


# get line, get word by word using split()
# txt = open("parse.txt" ,'r')
# for line in txt:					# get line
# 	for word in line.split():		# get word by word
# 		print word
# txt.close()


# get line, put words into list using split():  words = [word1, word2, .... wordn]
# txt = open("parse.txt" ,'r')
# for line in txt:					# get line
# 	words = line.split();			# get words = [word1, word2, .... wordn]
# 	print words[2]
# txt.close()



# split specify delimeter
# txt = open("parse.txt" ,'r')
# for line in txt:					# get line
# 	words = line.split(", ");			# get words = [word1, word2, .... wordn]
# 	print words[0]
# txt.close()
