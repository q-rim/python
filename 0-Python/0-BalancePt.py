#! /usr/bin/python

def balanceIndex(uIN):
	diff = []
	for i in range(len(uIN)):
		arrL = uIN[:i]
		arrR = uIN[i+1:]
		sumL = sum(arrL)
		sumR = sum(arrR)

		diff.append(abs(sumL - sumR))
		print arrL, uIN[i], arrR, "\t\t",diff

	return diff.index(min(diff))


# main
userIN = [1,2,3,4,5,6,7,8,9]
print balanceIndex(userIN)
