#! /usr/bin/python


# assertion:  shows additional error message outupt
#arg1 = 1
# arg1 = "test"
# print arg1
# assert(isinstance(arg1, str)), "not a string"


# # Exception
# try:
# 	#fh = open("/bin/2.txt", "w")		# don't have permission.  Will throw an exception
# 	fh = open("2.txt", "w")
# 	fh.write("exception handling TEST1")
# except IOError:
# 	print "Error:  no file or read data TEST1"
# else:
# 	print "Written content in the file succesfully"
# 	fh.close()



# try:
# 	#fh = open("/bin/2.txt", "w")		# don't have permission.  Will throw an exception
# 	fh = open("2.txt", "w")
# 	fh.write("exception handling TEST1")
# finally:
# 	print "Error:  no file or read data TEST2"


# Exception
try:
	#fh = open("/bin/2.txt", "w")		# don't have permission.  Will throw an exception
	fh = open("2.txt", "w")
	try:
		print "writing to file"
		fh.write("exception handling TEST1")
	finally:
		print "closing file"
		fh.close()
except IOError:
	print "Error:  no file or read data TEST1"
	
