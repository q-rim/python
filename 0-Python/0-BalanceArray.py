#! /usr/bin/python

# given an array of intergers, find the balancing point:

array = [1, 2, 4, 6, 7, 9, 36, 3, 25, 6, 3, 5, 3, 2, 34]
# i=len(arr)/2  # find midway point

def getBalancePt(arr):
	
	diff = []
	for i in range(len(arr)):
		arrL = arr[:i];		
		arrR = arr[i+1:];	

		sumL = sum(arrL); 	
		sumR = sum(arrR); 	
		diff.append(abs(sumL-sumR))

		print "arr["+str(i)+"] =", arr[i], "\t[arrL][arrR]=", arrL, arrR, "\tsumL=", sumL, "sumR=", sumR, "\tdiff=",diff

	print "minimum value:", min(diff)
	balPt = diff.index(min(diff))
	print "index of min diff = ", balPt
	return balPt

balancePt = getBalancePt(array)
print balancePt

