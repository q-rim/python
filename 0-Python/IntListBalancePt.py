#! /usr/bin/python

# find the balance point

intArr = [5, 4, 2, 6, 8, 23, 35, 7, 86, 34, 23, 7, 87, 35, 65, 78, 3, 97, 64, 4, 34, 43]
intArr = [5, 4, 2, 6, 8, 23, 35, 7, 86, 34, 23, 7, 87, 35, 65, 78, 3, 97, 64, 0, -52, 43]
# print intArr;

i=0;
balDiffList=[]
for val in intArr:
	left = sum(intArr[:i])
	right = sum(intArr[i:])
	balDiff = left - right
	balDiffList.append(balDiff)
	print "i=",i, "\tvalue:",val, "\t", intArr[0:i], "-----", intArr[i:len(intArr)], " \t----left sum:",left, "\tright sum:",right, "\tbalDiff (L - R):", balDiff
	i +=1

# get balance point.  This is going to be the lowest absolute value in the balDiffList.
# print "balDiffList:", balDiffList
balValue = map(abs, balDiffList)
# print "balValue:   ", balValue
balPoint = balValue.index(min(balValue))
print "Balance Point value:", min(balValue)
print "Balance Point index:", balPoint
print "Balance Point is between index:", balPoint-1, "," ,balPoint 
