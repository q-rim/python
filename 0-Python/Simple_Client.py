#!/usr/bin/python           # This is server.py file
# http://www.tutorialspoint.com/python/python_networking.htm

# Simple Network Client

import socket

s = socket.socket()
host = "127.0.0.1"
#host = socket.gethostname() # Get local machine name		<-- This will not work if you can't ping your own hostname
hostname = socket.gethostname() # Get local machine name		
print "hostname: "+hostname
port = 12345

s.connect((host, port))
print s.recv(1023)
s.close
