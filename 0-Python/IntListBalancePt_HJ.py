#!/bin/bash/python

def balance_point(input_list):
	a = input_list

	# Initialize lists

	list_left = []
	list_right = []
	balance_point_list = []

	
	for i in range(len(a)):	

		left = a[i]
		
		# Appends the value one by one to a new list (left part)
# [1]
# [1, 5]
# [1, 5, 0]

		list_left.append(left)

		# Calculates the sum of the current new list
		sum_left = sum(list_left)

		#print list_left
		#print sum_left

# Appends the value one by one to a new list (right part)
# [4, 2, 0]
# [4, 2]
# [4]

		list_right = a[i+2:len(a)]

		# Calculates the sum of the current new list
		sum_right = sum(list_right)

		#print list_right
		#print sum_right

		# If the left equals the right, return the index value between the left and the right part
		if sum_left == sum_right:
			balance_point = i + 1
			balance_point_list.append(balance_point)  # Makes a new list and stores all the balance points 

			print "Balance Point = Index", balance_point

	return balance_point_list     # Returns all the balance points

# main 

input_list = [1, 5, 0, 2, 4]

bp_list = balance_point(input_list)

print "The balance points are: ", bp_list