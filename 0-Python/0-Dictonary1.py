#! /usr/bin/python
# -*- coding: iso-8859-1 -*-

# Dictioanry = {key : value} pair
# hash mapping
# Dictionaries {key1:val1, key2:val2}

empty = {}
#empty["s"]


en_de = {"red":"rot", "green":"grün", "blue":"blau", "yellow":"gelb"}
en_de1 = {"house":"Haus", "cat":"Katze"}
print en_de["green"]
print en_de.has_key("green")

#en_de += en_de1	# cannot concatinate like list
en_de.update(en_de1)	# use dict1.update(dict2) method to merge the dictionaries.
print en_de
for key in en_de:
	print key, en_de[key]
 