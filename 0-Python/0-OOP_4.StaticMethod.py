#! /usr/bin/python

# constructor - initializies a method
# attributes --> characteristics (noun)
# methods    --> behaviors		 (verb)

# Static Methods() vs. Standard_Methods(self) in a Class.

# Declaring Methods:
# Static Method:          static()      <-- no argument needed.  Need to declare static via:  status = staticmethod(status)	
# NonStatic Method:   non_static(self)  <-- need "self" argument.  

# Calling Methods:
# Static Method:  	 You can call through Class or instance:   Critter.status();  or crit1.status();
# NonStatic Method:  You can only call through instance:   							 crit1.status();


class Critter(object):
	total = 0;

	# Static Method
	def status():						# static method: does not have "self" argument
		print "Static Method():  Critter Count: ", Critter.total
	status = staticmethod(status)		# static method: declaration

	# Non Static Method (self)
	def critterCount(self):				# non-static method:  uses "self" argument
		print "Non-Static Method():  Critter Count: ", Critter.total

	def __init__(self, name):
		Critter.total +=1;
		print "Critter created.  Critter count: ", Critter.total

# Main
crit1 = Critter("Kyurim")
crit2 = Critter("Booger")
crit3 = Critter(name="Nadja")

crit1.status()				# Static Method:  Can call by instance
Critter.status()			# Non-Static Method:  Can call by instance
crit1.critterCount()		# Static Method:  Can call by instance
#Critter.critterCount()		# Non-Static Method:  Can NOT call by instance		<-- does not work!

