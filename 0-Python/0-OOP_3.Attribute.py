#! /usr/bin/python

# constructor - initializies a method
# attributes --> characteristics (noun)
# methods    --> behaviors		 (verb)


class Critter(object):

	total = 0		# class Attribute = class variable

	def __init__(self, name):
		Critter.total +=1
		print "A new critter has been born.  Critter count: ", Critter.total
		self.name = name;
		print "new critter's name is:  "+ self.name


	def __str__(self):		# printing without this method will print the object's memory location
		text = "Critter object name: " + self.name
		return text

	def talk(self):
		print "I'm", self.name

crit1 = Critter("Kyurim")
crit2 = Critter("Bogger")
crit3 = Critter("Nadja")
print crit1
crit3.talk()

# Direct access of the attribute.  It should be avoided.  Instead use set/get method.
print Critter.total
print crit1.name
crit2.name = "Schizer"; 	print crit2.name