#! /usr/bin/python




# userIN = "text the best text"
# backwards = ""
# for i in range(len(userIN)-1, -1, -1):
# 	backwards += userIN[i]
# print backwards

# # find value "e"

# ePos = []
# countList = ""
# for i in range(len(userIN)):
# 	print "userIN["+str(i)+"] =", userIN[i]
# 	if userIN[i] == "e":
# 		ePos.append(i)



# print userIN
# print countList
# print "letter \"e\" is located in index: ", ePos




# # 2nd Try
def reverse_text(uIN):
	backwards = ""
	for char in uIN:
		backwards = char + backwards
	return backwards


def remove_vowels(uIN):
	VOWELS = "aeiouAEIOU"
	no_vowels = ""
	for char in uIN:
		if char not in VOWELS:
			no_vowels += char
	return no_vowels


userIN = "this is my text"
print reverse_text(userIN)

print remove_vowels(userIN)

