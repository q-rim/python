#! /usr/bin/python

# P.176
# global reach

def read_global():
	#print "From inside the local scope of read_global(), value is: ", value
	return value	### you can access global variable from inside the function

def shadow_global():
	value = -3		### you can create a new variable from a function without overwriting global variable.
	#print "From inside the local scope of shadow_global(), value is: ", value	
	return value

def change_global():
	global value	### you can write over the global variable from a function by declaring a global variable.
	value = -5
	#print "From inside the local scope of change_global(), value is: ", value
	return value

# main
value = 10
print "1: ", "\t\t\t\tGlobal: ", value 
print "2: ", "inside function: ", read_global(), "\tGlobal: ",  value
print "3: ", "inside function: ",shadow_global(), "\tGlobal: ", value
print "4: ", "inside function: ",change_global(), "\tGlobal: ", value
