#! /usr/bin/python
# Tuples are immutable
#  - You cannot delete or add to Tuples
# represented with ()

inventory = ("sword", "armor", "shield")
inventory1 = ("sword", "armor", "shield", "tsting", "absfdsd")

for x in inventory:
	print x


for i in range(len(inventory)):
	print inventory[i]

print inventory[0:2]



#grabbing a random item from a Tuple:
import random;
print "inventory is: ", inventory

selection = random.choice(inventory)
wrongAnswer = True;
while wrongAnswer == True:
	userIN = raw_input("guess what item was selected: ")
	if userIN != selection:
		print "wrong item"
	else:
		print "that is correct"
		wrongAnswer=False;


