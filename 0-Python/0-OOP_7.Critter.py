#! /usr/bin/python

class Critter(object):

	def __init__(self, name, hunger=5, boredom=3):
		self.name = name;
		self.hunger = hunger;
		self.boredom = boredom;


	def __str__(self):
		return "str():  name: " + self.name + "\thunger: " + str(self.hunger) + "\tboredom: "+ str(self.boredom) + "\tmood: " + self.mood


	def __pass_time(self):
		self.hunger +=1;
		self.boredom +=1;


	def __get_mood(self):
		unhappiness = self.hunger + self.boredom

		if unhappiness < 5:
			mood = "happy"
		elif 5<= unhappiness <= 10:
			mood = "okay"
		elif 11 <= unhappiness <= 15:
			mood = "frustrated"
		else:
			mood = "mad"
		return mood;
	mood = property(__get_mood);


	def talk(self):
		print "talk():  I'm talking here!!!"
		print(self)		# this will print __str__(method)
		self.__pass_time()


	def eat(self, food=4):
		print "eat():  hunger-"+str(food)
		self.hunger = max(self.hunger-food, 0)
		self.__pass_time()


	def play(self, fun=3):
		print "play():  boredom-"+str(fun)
		self.boredom -=fun
		self.__pass_time()



def main():
	# main
	crit = Critter("Kyurim")
	print crit
	# print crit.mood

	# crit.talk();	print crit
	# crit.eat(1); 	print crit
	# crit.eat(2); 	print crit
	# crit.eat(5); 	print crit

	# crit._Critter__pass_time(); 	print crit
	# crit._Critter__pass_time(); 	print crit
	# crit._Critter__pass_time(); 	print crit
	# crit._Critter__pass_time(); 	print crit

	# crit.play();	print crit
	# crit.play(5);	print crit
	# crit.play(6);	print crit

	
	foodIN = int(raw_input("Give food.  Enter the amount: "))
	crit.eat(foodIN);	print crit

main()