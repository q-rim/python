#! /usr/bin/python

from pprint import pprint

dns_count = {}  

j = 0

file = open("parsing_dns_count.txt", 'r')

for line in file:					# get line
	words = line.split()			# get word lin a line
	url = words[0]

	if url in dns_count:
		dns_count[url] += 1
	else:
		dns_count[url] = 1

# Print the output

template = "{0:18}{1:1}"
print template.format("url", "Count")

for i in range(len(dns_count)):
	print template.format(dns_count.items()[i][0], dns_count.items()[i][1])

