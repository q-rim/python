#! /bin/bash

pwd
# find out total number of directories
NUM_OF_DIR=`pwd | awk -F\/ '{print NF-1}'`
#echo "Total number of \"/\" = " $NUM_OF_DIR

OUT="";
for i in $(seq 1 $NUM_OF_DIR);do 
	j=`expr $i + 1`
	#echo j=$j;
	OUT_INT=`pwd | cut -d'/' -f$j | cut -c -1`
	OUT=$OUT/$OUT_INT;
	#echo $OUT;
done
echo $OUT
