#! /usr/bin/python

# constructor - initializies a method
# attributes --> characteristics (noun)
# methods    --> behaviors		 (verb)

class Critter(object):
	""" A virtual pet """

	# object method - function associated with the object.
	def talk(self):		
		print "Object method:  Hello World"
	
	# constructor method - used to initialize the object.  
	# Runs each time an object is created
	def __init__(self):		
		print "constructor method:  Hello World  <-- runs each time an oject is created"

	def __str__(self):
		return "constructor str method:  Hello, I'm a critter  <-- runs when the object is treated like a str"

crit = Critter()
crit.talk()
print crit

