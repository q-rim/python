#! /usr/bin/python
import random;

# [10,20,10,15,45,20,10,10]

# objective is to sort and find last index of each duplicate element

# last index of 10 is 3
# last index of 20 is 7


#1. sort arr
#2. if duplicate, find index of last duplicate.


def sort(arr):
	arrSorted = [arr[0]]
	for i in range(1, len(arr)):
		for j in range(len(arrSorted)):
			if arr[i] <= arrSorted[j]:						
				arrSorted = arrSorted[:j] + [arr[i]] + arrSorted[j:]; 	break
			else:										# Case where the arr[i] needs to be inserted to the last position of the arrSorted
				if (arr[i] >= max(arrSorted)):
					arrSorted.append(arr[i]); 	break
	return arrSorted


def indexOfChange(arr):
	arrIndex = []
	for i in range(len(arr)-1):
		if arr[i] != arr[i+1]:		
			arrIndex.append(i)
	if arr[0] == arr[len(arr)-1]:					# Handles corner case when all the values are the same.
		arrIndex.append(len(arr)-1)
	elif arrIndex[len(arrIndex)-1] != len(arr):		# Handles the last index value		
		arrIndex.append(len(arr)-1)
	return arrIndex
		

# Main
#arr = [1,2,3,4,5,6]
#arr = [1,1,1,1,1,1]
#arr = [0,0,0,0,0,0,0,0]
#arr = [10,10,10,10,15,20,20,20,45]
#arr = [10,10,-10,-10,15,20,-20,-20,-45]
#arr = [10,20,10,15,45,20,10,10]
#arr = [10, 30, 70, 80, 90, 90, 90]
arr = [10*random.randrange(-5,5,1) for _ in range (10)]

print "\nunsorted arr: \t", arr
arr = sort(arr)
print "sorted arr: \t", arr
indexValues = indexOfChange(arr)
print "indexes of the changes: ", indexValues, '\n'

