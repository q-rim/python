#! /usr/bin/python
# useage:  PingTEST.py <ip1> <ip2> ... <ipn>
# checks if the host is up or not

import StringIO;
import subprocess;
import sys;

def getHostStat():
	"""	Reads shell output a line at a time and parses the line for key word.
		arg in: output from ping"""

	global output
		
	foundStat = False;
	while foundStat == False:
		if output[0:3] == "---":
			# find hostname
			host_name = output[4:output.index(" ping")]; 		#print "HOSTNAME:", host_name
			# remove one line and update output
			indexEOL = output.index('\n');  	
			output = output[indexEOL:]; 
			#print "\n---------------------\n", output, "---------------------\n"
			# find host state: up/down
			indexEOL = output.index("packet loss");  			#print "###STATE: ", output[0:indexEOL]
			if "1 packets received" in output[0:indexEOL]:
			 	print host_name, "- UP"
			else:
			 	print host_name, "- DOWN"

			foundStat = True;
			
		else:
			# remove one line
			indexEOL = output.index('\n');  	
			if indexEOL==0:
				indexEOL=1;					#print "indexEOL: ", indexEOL;
			output = output[indexEOL:]; 
			#print "\n---------------------\n", output, "---------------------\n"
			
# Main
# Passing in variables
totalARG = len(sys.argv); 		#print "totalARG = ", totalARG
hostlist = str(sys.argv);		#print "hostlist = ", hostlist;
#print "1st argv = ", str(sys.argv[0])
#print "2nd argv = ", str(sys.argv[1])
#print "3rd argv = ", str(sys.argv[2])

# Generate the Shell Script Command
CMD = "";
for i in range(1,totalARG):
	# print i, str(sys.argv[i])
	CMD = CMD + "ping -c 3 -i 0.2 -o " + str(sys.argv[i]) + " & "

# Interacting with Shell using .Popen
p = subprocess.Popen(CMD, stdout=subprocess.PIPE, shell=True)
(output, err) = p.communicate()

# Interacting with Shell using commands.getstatusoutput()
# import commands
# print commands.getstatusoutput('wc -l file')
# output = commands.getstatusoutput(CMD)

# Execute
for i in range(1,totalARG):
	getHostStat()

