#! /bin/bash

# Usage:
# firstLetterOfDir.sh <IP1> <IP2> ... <IPn>

echo "Enter username for the servers: "
read USERNAME

for var in "$@";do
	HOSTLIST+=("$var")
done

TOTAL_MEM="0";
for i in "${HOSTLIST[@]}";do
	echo; 
	MEM=`ssh $USERNAME@$i -t cat /proc/meminfo | grep MemTotal` > /dev/null 2>&1;
	MEM=`echo $MEM | cut -d':' -f2`
	MEM=`echo $MEM | cut -d' ' -f1`
	let MEM=$MEM/1000000;
	echo Server: $i total mem: $MEM GB
	TOTAL_MEM=`expr $TOTAL_MEM + $MEM`
done

echo;
echo Combined Memory for all servers: $TOTAL_MEM GB