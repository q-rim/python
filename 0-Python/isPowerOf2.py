#! /usr/bin/python
import sys;

def isPowerOf2(testVal):
	i=0;
	while True:
		exp = 2**i
		if exp == testVal:
			return True
		if exp > testVal:
			return False
		i +=1					# putting the += 1 at the end handles the 2^0=1 case.

# main
# check to see if user prompt is neede or run direct from CLI.
if len(sys.argv) <= 1:
	argX = input("enter a test value:  ")
else:
	argX = int(sys.argv[1])

# output
if isPowerOf2(argX):
	print argX, "is a power of 2\n"
else:
	print argX, "is not a power of 2\n"
