#! /usr/bin/python

class Critter(object):

	def __init__(self, name):
		self.__name = name

	def get_name(self):
		return self.__name

	def set_name(self, name):
		if name == "":
			print "set_name():  Name cannot be an empty string."
		else:
			self.__name = name
			print "set_name():  New name: ", self.__name

	def talk(self):
		print "talk():  Hi, my name is: ", self.__name

	name = property(get_name, set_name, talk)			# property() function allows for easy access to get() set() methods

# Main
crit1 = Critter("Kyurim");		print crit1.get_name()
crit1.set_name("booger");		print crit1.get_name()
crit1.talk()


print "-----------------------------------"


# Using property(get_name, set_name) to get/set values.
crit1.name = "snot";			print crit1.name
crit1.talk		# Strange, does not seem to work for crit1.talk

