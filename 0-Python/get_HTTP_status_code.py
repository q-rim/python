#! /usr/bin/python
# HTTP Status Code

import httplib;
import subprocess;
import sys;

destIP = "192.168.1.55"

for i in range(1000):
	def get_status_code(host, path="/"):
	    """ This function retreives the status code of a website by requesting
	        HEAD data from the host. This means that it only requests the headers.
	        If the host cannot be reached or something else goes wrong, it returns
	        None instead.
	    """
	    try:
	        conn = httplib.HTTPConnection(host)
	        conn.request("HEAD", path)
	        return conn.getresponse().status
	    except StandardError:
	        return None


	#print get_status_code("q-rim.com") # prints 200
	#print get_status_code("q-rim.com/what") # prints 404
	httpCode = get_status_code(destIP)
	print "Python:\tDestination:", destIP, "HTTP Status Code:", httpCode
	if httpCode == 404 and httpCode == 400:
		sys.exit("Server output: 404")




	# Using Shell:
	CMD = "wget --spider -S " +destIP+ " 2>&1 | grep HTTP/ | awk '{print $2}'"
	p = subprocess.Popen(CMD, stdout=subprocess.PIPE, shell=True)
	(output, err) = p.communicate()

	print "SHELL: \tDestination:", destIP, "HTTP Status Code:", output

	if output == "404":
		sys.exit("Server output: 404")
