#! /usr/bin/python
# Dictioanry = {key : value} pair
# hash mapping
# Dictionaries {key1:val1, key2:val2}

httpcode = {"200" : "HTTP Code - OK/Success",
		"301" : "HTTP Code - Permanent",
		"302" : "HTTP Code - Temporary",
		"404" : "HTTP Code - Not Found",
		"500" : "HTTP Code - Server Error",
		"503" : "HTTP Code - Unavailable",
}


# add to the dictionary
# update a value
httpcode["234"] = "HTTP Code - test"; print httpcode

# get the value using key
print httpcode["234"]
print httpcode.get("301")


# get key using value.  
# There's no easy functions for this.  You have to iterate through the dictionary.
# get key of value "HTTP Code - OK/Success"
#test_key = "HTTP Code - OK/Success"

def getDictionaryValue(dictionary, test_key):
	for key in dictionary:
		if dictionary[key] == test_key:
			return key

key = "HTTP Code - Temporary"
print getDictionaryValue(httpcode, key)


# check if key exists.  There is no method.  
print httpcode.has_key("200")


# return only keys
print httpcode.keys()
# return only values
print httpcode.values()

# puts each key:value pair into List.  Why??? I don't know.
print httpcode.items()

# delete element
del httpcode["234"]
# delete all entries
httpcode.clear()
# delete entire dictionary
del httpcode

