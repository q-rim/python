#! /usr/bin/python

# Palindrome
import sys;

def isPalindrome(wordIN):
	"""Checks to see if the wordIN is Palindrome.  Returns True/False"""
	l=len(wordIN);
	# print "l=", l; 		print "l/2-1=", l/2
	for i in range(0, l/2):
		#print "i=",i; 		print wordIN[i]; 		print wordIN[l-i-1]
		if wordIN[i] == wordIN[l-i-1]:
			#print wordIN, "is a Palindrome"
			pass
		else:
			#print wordIN, "is not a Palindrome"
			return False
	return True


# Main
# pass in argument from CLI
word = str(sys.argv[1]); 		#print wordIN
pal = isPalindrome(word);

if pal:
	print word, "is a Palindrome";
else:
	print word, "is a not Palindrome";

