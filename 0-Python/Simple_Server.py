#!/usr/bin/python           # This is server.py file
# http://www.tutorialspoint.com/python/python_networking.htm

import socket               # Import socket module

s = socket.socket()         # Create a socket object

host = "127.0.0.1"
#host = socket.gethostname() # Get local machine name		<-- This will not work if you can't ping your own hostname
hostname = socket.gethostname() # Get local machine name		
print "hostname: "+hostname

port = 12345                # Reserve a port for your service.
s.bind((host, port))        # Bind to the port

print "Sever Listening:\thostname:", hostname, "\tport:", port

s.listen(5)                 # Now wait for client connection.
connection_count=1
while True:
   c, addr = s.accept()     # Establish connection with client.
   print  'Server: '+hostname+'\tConnection: '+str(connection_count)+'\tGot connection from', addr
   c.send('Server: '+hostname+'\tConnection: '+str(connection_count)+'\tThank you for connecting to server :)\n')
   c.close()                # Close the connection			
   connection_count +=1