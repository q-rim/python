#! /usr/bin/python

# List

inventory = ["sword", 
			"armor", 
			"shield", 
			"healing potion", 
			"boots"];

print inventory
print inventory[0:2]
print inventory[2:3]

# replacing a value in List
inventory[0] = "fork";			print inventory

# find Index of the value
inventory.index("shield")

# append to a list
inventory += ["1newVal"]; 		print inventory
inventory.append("2newVal");	print inventory

# add a new value in a List
inventory = inventory[:1] + ["test"] + inventory[1:];		print inventory

# delete an element in List
del inventory[7];			 	print inventory
del inventory[1:2];				print inventory
inventory = inventory[:1] + inventory[2:]; 		print inventory


# for loop
for i in inventory:
	print i


for i in range(len(inventory)):
	print inventory[i]




# # Nested Sequences
nested = ["first", ("second", "thrid"), ["fourth", "fifth", "sixth"]]
print nested
print nested[0]
print nested[1]
print nested[2]
print nested[2][1]

# scores = [("Moe", 1000), ("Larry", 1500), ("Curly", 3000)];
# print scores
# name, score = scores[0]
# print name
# print score

# # Shared Reference
# kyurim = ["jeans", "t-shirt", "jacket"];
# honey = kyurim
# kyu = kyurim

# print kyurim
# print honey
# print kyu
# print ""

# # I change one value and they all change.  Shared reference.
# honey[1] = "dress shirt"
# print kyurim
# print honey
# print kyu
# print ""

# # To avoid this, you have to create a new duplicate variable.
# newKyurim = kyurim[:]
# print newKyurim
# newKyurim[1] = "flannel"
# print kyurim
# print newKyurim