#! /usr/bin/python

# Lists
# Lists: []
# Tuples: ()
# Dictonaries: { key:definition}

boots = "boots"
inventoryDictionary = ("sword", 
			"armor", 
			"shield", 
			"healing potion", 
			boots);
print "Dictonary = \t", inventoryDictionary

# Create List
inventory = ["sword", 
			"armor", 
			"shield", 
			"healing potion", 
			boots];
print "Lists = \t", inventory

# len(list)
print "length of list: ", len(inventory);

# using "in" operator
item = "sword"
if item in inventory:
	print "This item is in the list: ", item;
else:
	print "This item is not in the list: ", item;

# Indexing
print "inventory[0] = ", inventory[0]
print "inventory[len(inventory)-1] = ", inventory[len(inventory)-1]
print "inventory[0:1] = ", inventory[0:1]
print "inventory[2:4] = ", inventory[2:4]

print inventory.index("shield")		# get index of the shield

# Concatinate
box = ["axe", "gold"]
print inventory + box

# Assignment of new element
inventory[1] = "t-shirt"
print inventory;

inventory[1:3] = ["orb of future telling"]
print inventory;

# delete element
del inventory[3]
print inventory;

del inventory[1:5]
print inventory;


# Nested Sequences
nested = ["first", ("second", "thrid"), ["fourth", "fifth", "sixth"]]
print nested
print nested[0]
print nested[1]
print nested[2]
print nested[2][1]

scores = [("Moe", 1000), ("Larry", 1500), ("Curly", 3000)];
print scores
name, score = scores[0]
print name
print score

# Shared Reference
kyurim = ["jeans", "t-shirt", "jacket"];
honey = kyurim
kyu = kyurim

print kyurim
print honey
print kyu
print ""

# I change one value and they all change.  Shared reference.
honey[1] = "dress shirt"
print kyurim
print honey
print kyu
print ""

# To avoid this, you have to create a new duplicate variable.
newKyurim = kyurim[:]
print newKyurim
newKyurim[1] = "flannel"
print kyurim
print newKyurim