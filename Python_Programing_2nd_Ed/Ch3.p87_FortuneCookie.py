#! /usr/bin/python

# this program simulates a fortune cookie.
# displays one of 5 different messages.

import random;

fortune1 = "Ships are save in harbor, but that's not what ships are made for";
fortune2 = "Knowing is not enough.  You must APPLY.\nWilling is not enough.  You must DO!";
fortune3 = "You are the gatekeeper of your Destiny";
fortune4 = "Excellence is not an act, but a Habbit";
fortune5 = "Nothing worth doing is ever easy";

raw_input("Press ENTER to open your fortune cookie.");

moreFortune=True;
while moreFortune==True:
	selection = random.randrange(5)+1;
	if selection==1:
		print("Fortune: \n\""+fortune1+"\"");
	elif selection==2:
		print("Fortune: \n\""+fortune2+"\"");
	elif selection==3:
		print("Fortune: \n\""+fortune3+"\"");
	elif selection==4:
		print("Fortune: \n\""+fortune4+"\"");
	elif selection==5:
		print("Fortune: \n\""+fortune5+"\"");
	else:
		print("ERROR");

	moreIN = raw_input("\nWould you like to see another Fortune?  y/n: ");
	print moreIN;
	if moreIN == "y":
		print("Next Fortune.");
	elif moreIN == "n":
		print("Exiting.");
		moreFortune = False;
	elif moreINF == "\n":
		print("EnTER?");
	else:
		print("Input Error");

