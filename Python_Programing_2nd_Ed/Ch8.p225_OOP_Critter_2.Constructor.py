#! /usr/bin/python

# Simple Critter
# Constructor
# Class methods

class Critter(object):			# base my class on "object"
	"""A Virtual Pet"""
	
	def __init__(self):								# Constructor = Initialization Method() - automatically runs each time a critter object is created.  It "Constructs" a critter object.
		print "A new critter has been born!"

	def __str__(self):
		return "Hi.  I'm an instance of class Critter.  Critter has been created!"

	def talk(self):				# def method = function associated with an object.
		print(self)


# main
crit1 = Critter()				# Instantiate a Critter Object.
crit2 = Critter()				# Instantiate a Critter Object.
crit2.talk()						# invoke crit()'s talk method.
print crit1

