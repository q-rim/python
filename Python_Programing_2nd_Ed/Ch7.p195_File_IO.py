#! /usr/bin/python

# File I/O
# NOTE:  You have to open/close file each time as it will only make one pass through the file.

# Read the whole File
# txt.read()

import urllib2
import string

text_file = open("Ch7.p195_File_IO.txt", "r");
whole_txt = text_file.read();		
text_file.close();			
print "##### Whole text: \n", whole_txt;

# read a line at a time
# txt.readline()
text_file = open("Ch7.p195_File_IO.txt", "r");
line1 = text_file.readline(); 	
line2 = text_file.readline(); 	
line3 = text_file.readline(); 	
text_file.close();			
print "##### line of text: \n"+line1+line2+line3

# Read Charactors in a line.  
# txt.readline(char#)     - NOTE:  This will read to the end of the top line only.
# txt.read(char#)         - NOTE:  This will read to the end of the file.
text_file = open("Ch7.p195_File_IO.txt", "r");
char1 = text_file.readline(1); 	
char2 = text_file.readline(5); 	
char3 = text_file.readline(40); 	
text_file.close();			


print "\n------line1: " + line1
print "\n------line1: " + line1
print "\n------line1: " + line1
print "\n------line1: " + line1
print "\n------line1: " + line1
print "\n------line1: " + line1

print "\n------line1.split(): " + line1.split()




print "##### Charactors:", "\nchar1 = "+char1, "\nchar2 = "+char2, "\nchar3 = "+char3
print "char1+char2+char3 = ", char1+char2+char3

# Read entire file into a LIST each line as element:  [ line1, line2, line3, ... line_n ]
# txt.readlines()
text_file = open("Ch7.p195_File_IO.txt", "r");
lines = text_file.readlines(); 	
text_file.close();			
print lines;

for line in lines:
	print line

# Looping through the file, line by line
text_file = open("Ch7.p195_File_IO.txt", "r");
for line in text_file:
	print line
text_file.close();			


# Writing to a file:   use "w" flag
# Appending to a file: use "a" flag
text_file = open("Ch7.p195_File_IO.txt", "a+");
text_file.write("Line4:  Depression breeds depression.  Effort breeds SUCESS!\n")
text_file.write("Line5:  Believe in me, who beleives in you\n")
whole_txt = text_file.read();
text_file.close();			
print "##### Append: \n", whole_txt;

