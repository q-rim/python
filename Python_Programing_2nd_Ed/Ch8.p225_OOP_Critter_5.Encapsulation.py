#! /usr/bin/python


# NOTE:  private is denoted with leading "__":
# __private_attribute
# __private_method(self):


# Object Encapsulation - Object's privacy.  Got to respect Object's privacy!

# private Attributes:  self.__private_attribute
#     should be accessed by class method.  They shouldn't be accessed directly.  
#	  However, you can still access it:  crit._Critter__mood

# private Methods:  self.__private_method()
#     should also be accessed by class method.
#	  However, you can still access it:  crit._Critter)__private()

class Critter(object):			# base my class on "object"
	"""A Virtual Pet"""

	def __init__(self, name, mood):
		print "Critter born!"
		self.name = name					# public  Attribute
		self.__mood = mood 					# private Attribute:   self.__private_attribute

	def talk(self):
		print "\nI'm", self.name	
		print "Right now I feel", self.__mood, "\n"		# Accessing private Attribute through a public method

	def __private_method(self):							# private method
		print "This is private method()"

	def public_method(self):				
		print "This is public method()"
		self.__private_method()				# Calling private method through public method:  self.__private_method()


# Main
crit = Critter(name="Booger", mood="happy")

# Directly Accessing Private Method - Not recommended directly accessing private attribute/methods.
# crit.__private_method()			# This does not work
# Critter.__private_method()		# This does not work
# Critter.public_method()			# public method cannot be called via a class.  It has to be called via object.
crit._Critter__private_method()		# Accessing private method:				 obj._Class__<method>()

# Accessing private Attribute. 
print crit._Critter__mood			# Accessing private Attribute:   value = obj._Class__<attribute>

# Best Practice:  Access private method/Attribute through a public method.
crit.public_method()				# Accessing the private method through a public method - must call through an object.
crit.talk()


