#! /usr/bin/python

# Dictionaries {key1:def1, key2:def2}

geek = {"200" : "Q-OK/Success",
		"301" : "Q-Permanent",
		"302" : "Q-Temporary",
		"404" : "Q-Not Found",
		"500" : "Q-Server Error",
		"503" : "Q-Unavailable",
}

# Get Definition
print geek["404"];
print geek.get("404")
# print geek["707"]  				# <-- if key does not exist in Dictionary, returns Error
print geek.get("707")				# <-- if key does not exist in Dictionary, returns string "none"
print geek.get("707", "My_Memo")	# <-- if key does not exist in Dictionary, returns string "My_Memo"

# Checking the existance of key with "in" operator.
print geek.has_key("404")
# My own custom checker:
def keyExists(key="707"):
	if key in geek:
		return True;
	else:
		return False;

print keyExists()
print keyExists(key="404")


# Add an item
geek["key1"] = "def1"
print geek

# Write over an item
geek["key1"] = "def2"
print geek

# Delete an item
# if you del item that doesn't exist, you will get error.
del geek["key1"]
print geek

# Return only keys
print geek.keys()

# Return only values
print geek.values()

# Return items
print "\nitems: ", geek.items()
