#! /usr/bin/python

# Tuples
# Tuples are immutable.

boots = "boots"
inventory = ("sword", 
			"armor", 
			"shield", 
			"healing potion", 
			boots);
print inventory


# looping through Tuple elements:
for element in inventory:
	print element;

print "len(inventory) = ", len(inventory)


# using in operator
item = "boots";
if item in inventory:
	print item + " is in the inventory"
else:
	print "not there"


# Index of Tuples
print "indexing:  inventory[0]: ", inventory[0]
print "indexing:  inventory[1]: ", inventory[1]
print "indexing:  inventory[0:3]: ", inventory[0:3]
print "indexing:  inventory[0:14]: ", inventory[0:14]

# immutable
#inventory[2] = "axe"

# Returns error:
# Traceback (most recent call last):
#   File "./Ch4.p113_Tuples.py", line 37, in <module>
#     inventory[2] = "axe"
# TypeError: 'tuple' object does not support item assignment

chest = ("axe", "desk")
print "chest = ", chest
inventChest = inventory + chest;
print "inventory + chest = ", inventChest;


import random;
endGame=False;
while endGame == False:
	
	print random.choice(inventChest);

	uin = raw_input("Press \"n\" if you wish to quit: ");
	if uin=="n":
		print "this"
		endGame = True;
		

		



