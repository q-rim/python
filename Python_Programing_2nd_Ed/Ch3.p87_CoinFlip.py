#! /usr/bin/python

# Coin Flip

import random;

numOfFlips = int(raw_input("Enter the number of coin flips: "))+1;

i=1; seedSum=0;
for i in range(1, numOfFlips):
	
	seed = random.uniform(0,1);
	coin = round(seed);
	if coin < 0.5:
		print("Coin_Flip"+str(i)+": Heads");
	else:
		print("Coin_Flip"+str(i)+": Tails");
	i+=1;
	seedSum += seed;
print("average value: " + str(seedSum/i));

raw_input("Press ENTER to exit");