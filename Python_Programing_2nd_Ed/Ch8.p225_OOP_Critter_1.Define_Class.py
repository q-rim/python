#! /usr/bin/python

"""
OP (Object Oriented Programing)
	Todo OOP
		1. create Class to define Objects.
		2. Write Methods and create atrributes for objects.
		3. Instantiate objects from classes.

"""

# Simple Critter
# Demonstrates basic class and object
class Critter(object):			# base my class on "object"
#class Critter:			Old school Object declaration.  Still works, but let's use the new way.
	"""A Virtual Pet"""
	def talk(self):				# def method = function associated with an object.
		print "Hi.  I'm an instance of class Critter.  Critter has been created!"

# main
crit = Critter()				# Instantiate a Critter Object.
crit.talk()						# invoke crit()'s talk method.


