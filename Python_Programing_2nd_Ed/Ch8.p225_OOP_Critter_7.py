#! /usr/bin/python

# Final Critter Class
import random;

class Critter(object):
	"""A virtual pet"""
	
	def __init__(self, name, hunger=5, boredom=3):
		self.name = name;
		self.hunger = hunger;
		self.boredom = boredom;

	def __str__(self):
		return "\nTalking - critter_name: "+self.name+"\tmood: "+str(self.mood)+"\thunger: "+str(self.hunger)+"\tboredom: "+str(self.boredom)

	
	def __pass_time(self):
		self.hunger +=1;
		self.boredom +=1;

	
	def __get_mood(self):

		unhappiness = self.hunger + self.boredom
		#print "unhappiness:", unhappiness
		if unhappiness < 5:
			mood = "happy"
		elif 5<= unhappiness <=10:
			mood = "okay"
		elif 11<= unhappiness <= 15:
			mood = "frustrated"
		else:
			mood = "mad"
		return mood;
	mood = property(__get_mood);


	def talk(self):
		print(self)
		self.__pass_time()

	
	def eat(self, food=4):
		print "Eating:  Burp!  Thank you!"
		self.hunger -=food;
		if self.hunger <=0:
			self.hunger = 0;
		self.__pass_time()

	
	def play(self, fun=4):
		print "Playing: Wheee!"
		self.boredom -=fun;
		if self.boredom<=0:
			self.boredom=0;
		self.__pass_time()


def main():
	crit_name = raw_input("What do you want to name your critter?:")
	crit = Critter(name=crit_name)

	choice = None
	while choice !="0":
		print \
		"""
		Critter Caretaker:
		0. quit
		1. Listen to your Critter
		2. Feed your Critter
		3. Play with your Critter
		"""

		choice = raw_input("Choice: "); print


		if choice=="0":
			print "Good Bye! :)"
		elif choice=="1":
			crit.talk()
		elif choice=="2":
			crit.eat()
		elif choice=="3":
			crit.play()
		else:
			print "Not valid choice.  Try again."


main()

# crit = Critter(name="Booger", hunger=10, boredom=2);
# print crit.mood

# crit._Critter__pass_time()
# print crit.mood
# crit.talk()

# crit._Critter__pass_time()
# print crit.mood
# crit.talk()

# crit.eat()
# crit.talk()

# crit.play()
# crit.talk()


# print crit