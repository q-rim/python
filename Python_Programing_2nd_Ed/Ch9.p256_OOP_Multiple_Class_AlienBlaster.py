#! /usr/bin/python

# Alien Blaster
# Demonstrates object interaction

# NOTE:  It is POLITE to directly access another method(), but not attribute.

class Player(object):
	""" A player in a shooter game """

	def blast(self, enemy):
		print "Player.blast():  The player blasts an enemy."
		enemy.die()			# envokes ARG.die() method.


class Alien(object):
	""" An alien in a shooter game """

	def die(self):
		print "Alien.die():  I'm dying!"


# main

hero = Player()
invader = Alien()
hero.blast(invader)

