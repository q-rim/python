#!/usr/bin/python

#count to 10, but skip 5

count = 0
while True:
	count +=1
	if count > 10:
		break			# end program
	
	if count == 5:
		continue		# Jump to top of the loop

	print count


