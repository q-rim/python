#! /usr/bin/python

# Exception

# "try" a block of code to see if it creates an exception.

# NOTE: Excpetion handling is a good idea when you have varying inputs. i.e., userInput, textFile_input, reading variables from external source.
	# Computers don't cause errors, humans do.

# NOTE: To find out what Type of Exception you might need to catch, just generate the exception: i.e., run 1/0 to see what exception is generated.
# Possible Exceptions:
	# IOError
	# IndexError
	# KeyError
	# NameError
	# SyntaxError
	# TypeError
	# ValueError
	# ZeroDivisionError



# try/except
# Catch All Excpetion
try:
	num = float(raw_input("1. Enter a number: "));		# only accepts numbers.
except:								# Catch ALL Exceptions - not ideal
	print "Exception: you must enter a number"

# try/except(ValueError):  specifying the exception
# Catch only ValueError Exception
try:
	num = float(raw_input("2. Enter a number: "));		# only accepts numbers.
except(ValueError):					# Catch ValueError - good practice to specify type of Exception.
	print "Exception: you must enter a number"


# Catch Multiple Excpetion
# try/except(ValueError, TypeError):  specifying multiple possible exceptions
try:
	num = float(raw_input("3. Enter a number: "));		# only accepts numbers.
except(ValueError, TypeError):		# Catch multiple excpetions - good practice to specify type of Exception.
	print "Exception: you must enter a number"


# Exception Argument
# When exception occurs, it may have an associated value = Exception Value
try:
	num = float(raw_input("4. Enter a number: "));		# only accepts numbers.
except(ValueError, TypeError), e:		# 
	print "Exception: you must enter a number.  \nException:\n", e 




# Using try/except block in while loop:
isNum = False;
while isNum == False:
	try:
		num = float(raw_input("0. Enter a number: "));		# only accepts numbers.
		isNum = True;
	except(ValueError, TypeError), e:
		print "Exception: you must enter a number.\nException:\n", e
	else:													# else statement if no Exception is caught.
		print "The number you've entered is: ", num;



