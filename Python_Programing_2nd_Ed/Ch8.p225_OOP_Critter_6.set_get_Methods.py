#! /usr/bin/python

# Limit access to the Object by using set() get() methods
# perperty() method

class Critter(object):
	"""Class Definition"""
	
	def __init__(self, name):
		print "A new critter has been born!"
		self.__name = name;			# private attribute
		
	def get_name(self):				# get() - correct way of accessing private attribute.
		return self.__name			

	def set_name(self, name):		# set() - correct way of setting private attribute.
		if name=="":
			print "Name cannot be empty string."
		else:
			self.__name = name;			
			print "Name changed to: ", self.__name

	def talk(self):
		print "Hi, I'm ", self.__name

	name = property(get_name, set_name, talk)		# property() function allows for easy access to the get(), set() methods.

print "\n"
crit1 = Critter(name="Booger")
crit1.talk()
print crit1.get_name()

print "\n"
crit1.set_name(name="")
print crit1.get_name()

print "\n"
crit1.set_name(name="Loogie")
print crit1.get_name()

# using property to get/set values.
print "\nAccessing __name via property()"
print crit1.name
crit1.name = "Snot"
print crit1.name

