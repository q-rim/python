#! /usr/bin/python

# Guess My number Game.
# Prompts to keep guessing the number till the correct number is guessed.

import random;

# set Random number
number = random.randrange(100)+1;
print "TESTING: the random number = " +str(number) + "\n\n";


tries = 1;
iscorrect = False;
while iscorrect==False:

	# prompt user for a number:
	user_in = raw_input("Guess a number between 1-100: ");
	print "you have entered: " + user_in;
	user_in = int(user_in);

	# number comparison:
	if user_in<number:
		print "you have guessed too low";
	elif user_in>number:
		print "you have guessed too high";
	elif user_in==number:
		print "\nyou have guessed correct number!  Good Job!!! :)";
		print "It took you " + str(tries) + " tries";
		iscorrect = True;
	else:
		print "input error";

	tries += 1;

raw_input("\n\nPress ENTER to quit");