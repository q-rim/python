#! /usr/bin/python

"""
OP (Object Oriented Programing)
	Todo OOP
		1. create Class to define Objects.
		2. Write Methods and create atrributes for objects.
		3. Instantiate objects from classes.
		4. Restrict access to an object's atrributes.
		5. Work with both new-style and old-style classes.

"""

# Simple Critter
# Demonstrates basic class and object
class Critter(object):
#class Critter:			Old school Object declaration.  Still works, but let's use the new way.
	""" A Virtual Pet """

	total = 0;
	
	def status():					# Static Method.  Note no (self) parameter. 	Static method is a Class method.
		print "\nTotal # of critter: ", Critter.total
	status = staticmethod(status);	# Declare Staticmethod.


	def __init__(self, name, mood):		# Constructor = Initialization Method() - automatically runs each time a critter object is created.  It "Constructs" a critter object.
		print "A new critter is born!"
		self.name = name;				# public Attribute
		self.__mood = mood;				# private __Attribute
		Critter.total +=1;

	def __str__(self):				# special __str__ method allows the object to be printed.  Without this, it will print memory location.
		rep = "Critter object name: " +self.name + "\n"
		return rep

	def talk(self):
		print "Hi. I'm an instance of class Critter."
		print "My __mood is: ", self.__mood, "\n"		# access private __Attribute

	def sayHi(self):
		print "Hello!  It's me! :)"

	def __private_method(self):		# private method()
		print "THis is a private method"

	def public_method(self):		# how to use private method.  use it through a public method.
		print "This is a public method."
		self.__private_method()

# Main
Critter.status()									# call Class static Method.  Static methods are invoked through Class.
print "Critter total: ", Critter.total 				# call Class variable via Class.
crit1 = Critter(name="1-Poochie", mood="shitty")	# Create an instance of Critter.  Instantiating an object.
print "Critter total: ", crit1.total 				# call Class variable.
crit2 = Critter(name="2-Booger", mood="happy")		# Create an instance of Critter.  Instantiating an object.
print "Critter total: ", Critter.total 				# call Class variable.
crit2 = Critter(name="3-Loogie", mood="sad")		# Create an instance of Critter.  Instantiating an object.
print "Critter total: ", Critter.total 				# call Class variable.

crit1.talk()					# invoke a Critter's talk() method.
								# access private attribute through a public method()
crit2.sayHi()					# invoke a Critter's sayHi() method.

print crit2.name				# Access Critter Attribute
crit2.name = "Snot"; 		print crit2.name  	# Changing  Critter Attribute

print "crit1: ", crit1			# Access the __str__() method
print "crit2: ", crit2			


print "private attribute ", crit2._Critter__mood 				# access private attribute directly.
print "private method:",  	crit2._Critter__private_method()	# access private method directly
print "private attribute:", crit1.talk()						# access private attribute through a public method.
print "private method:", 	crit2.public_method()				# access private method through a public method.

Critter.status()						# Call Class static method


print "Critter total: ", Critter.total 	# Call Class variable via Class.
print "crit1 total: ", crit1.total 		# Call Class variable via object.
print "crit2 total: ", crit2.total 		

