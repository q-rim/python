#! /usr/bin/python

# guess my number with a computer.
# computer guesses once, I guess next.

import math;
import random;

# set Random number
highestNum=100;
number = random.randrange(highestNum)+1;
print("TESTING: the random number = " +str(number) + "\n\n");
print("This program finds a random number between 1-100");

tries = 1;
iscorrect = False;   	currNum=highestNum/2;	increment=currNum;
while iscorrect==False:

	# prompt user for a number:
	user_in = raw_input("Your Turn.  Guess a number between 1-100: ");
	print "you have entered: " + user_in;
	user_in = int(user_in);

	# number comparison:
	if user_in<number:
		print "you have guessed too low";
	elif user_in>number:
		print "you have guessed too high";
	elif user_in==number:
		print "\nyou have guessed correct number!  Good Job.  You beat the computer!!! :)";
		print "It took you " + str(tries) + " tries";
		iscorrect = True;
		break;
	else:
		print "input error";


	# Computer
	print("COMPUTER: checking number: " + str(currNum));
	if currNum==number:
		print("\nCOMPUTER: Found the number.  The number is: " +str(currNum));
		print("You lost to the computer. :(");
		iscorrect=True;
		break;

	elif currNum<number:
		print("COMPUTER: "+str(currNum) + " is too low");
		increment = math.ceil(increment/2);
		#print("increment: "+str(increment));
		currNum = currNum + increment;

	elif currNum>number:
		print("COMPUTER: "+str(currNum) + " is too high");
		increment = math.ceil(increment/2);
		#print("increment: "+str(increment));
		currNum = currNum - increment;

	tries += 1;

raw_input("\n\nPress ENTER to quit");


# Break this up into 2 functions.  
# 1. computer_turn() 
# 2. player_turn()