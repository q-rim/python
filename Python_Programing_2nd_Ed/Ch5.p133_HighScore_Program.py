#! /usr/bin/python

# Lists 
# Highscore Program

# List of scores
score = ["123", "99", "2", "12", "1"];

exit = False;
while exit == False:

	print \
	"""
	High Score Keeper

		0. Exit
		1. Show scores
		2. Add a score
		3. Delete a score
		4. Sort scores
	"""
	choice = raw_input("Choice: ")


	if choice == "0":
		print "0. exiting program"
		exit = True;
	elif choice == "1":
		print "1. Showing scores"
		print score;

	elif choice == "2":
		newScore = raw_input("2. Enter your new score: ")
		#score = score + [newScore];
		score.append(newScore);
		print score;

	elif choice == "3":
		delScore = raw_input("3. Enter the score to delete: ")
		if delScore in score:
			score.remove(delScore)	
			# finds the element and removes it.  What if there is 2 duplicate element?  
			# Deletes the first one in the list
			print score;
		else:
			print "The following score does not exist in the scores list: ", delScore

	elif choice == "4":
		print "4. Sort Scores"
		score = map(int, score);
		print score
		score.sort()
		score.reverse()
		score = map(str, score);
		print score

	else:
		print "wrong choice"
		exit = True;

