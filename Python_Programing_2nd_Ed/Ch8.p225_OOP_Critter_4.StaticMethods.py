#! /usr/bin/python

# Simple Critter
# Static Methods() vs. Standard_Methods(self) in a Class.

# Declaring Methods:
# Static Method:          static()      <-- no argument needed.  Need to declare static via:  status = staticmethod(status)	
# NonStatic Method:   non_static(self)  <-- need "self" argument.  

# Calling Methods:
# Static Method:  	 You can call through Class or instance:   Critter.status();  or crit1.status();
# NonStatic Method:  You can only call through instance:   							 crit1.status();


class Critter(object):			# base my class on "object"
	"""A Virtual Pet"""
	
	total = 0;


	# Static Method
	def status():												# Create Static Method
		print "STATIC:     Total number of Critter is:", Critter.total
	status = staticmethod(status)								# declare this Method Static

	
	# Non Static Method
	def critterCount(self):										# Non Static Method - need argument (self)
		print "NON-STATIC: Total number of Critter is:", Critter.total
	#critterCount = staticmethod(critterCount)					# No need to declare this Method Static.


	def __init__(self, name):
		Critter.total +=1;
		print "Critter born.  Critter#"+str(Critter.total)


# Main
crit1 = Critter("Booger")
crit2 = Critter("Snot")
crit3 = Critter(name="Loogie")


# Invoking Static Method
Critter.status()		# Calling Static Methods 	- Can call by Class 
crit1.status()			# Calling Static Methods 	- Can call by instance
#Critter.critterCount()	# Calling non-Static Method - Can NOT call by Class
crit1.critterCount()	# Calling non-Static Method - Can call by instance

