#!/usr/bin/python
# Quotation Manipulation
# Demonstration of String Methods

quote = "I Think there is a world market for maybe five computers."

print "Original quote:"
print quote

print "\nUpper Case:  \nquote.upper()"
print quote.upper()

print "\nLower Case:  \nquote.lower()"
print quote.lower()

print "\nLeading Letters Caps and all others are lower case:  \nquote.title()"
print quote.title()

print "\nReplace string:  \nquote.replace(\"five\", \"millions of\", [,max]) <- optional max is for number of max replacements"
print quote.replace("five", "millions of")

print "\nGet rid of tab, space and newlines at begining and end of the line are removed:  \nquote.strip()"
print quote.strip()


raw_input("\npress any key to exit")