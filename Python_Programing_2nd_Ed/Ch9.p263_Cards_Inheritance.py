#! /usr/bin/python

# Combining Objects
# Playing Cards

class Card(object):
	""" A deck of cards """
	RANKS = ["A", "2", "3", "4", "5", "6", "7", "8", "9", "J", "Q", "K"]
	SUITS = ["c", "d", "h", "s"] 	# clubs, diamonds, hearts, spades

	def __init__(self, rank, suit):
		self.rank = rank
		self.suit = suit

	def __str__(self):
		rep = self.rank + self.suit
		return rep


class Hand(object):
	""" Hand of a playing cards """

	def __init__(self):
		self.cards = []

	def __str__(self):
		if self.cards:			# if self.cards is not an empty string
			rep = ""
			for card in self.cards:
				rep += str(card) + " "
		else:
			rep = "<empty>"
		return rep

	def clear(self):
		self.cards = []

	def add(self, card):
		self.cards.append(card)				# .append()  -- this must be a standard method?

	def give(self, card, other_hand):
		self.cards.remove(card)				# .remove()  -- this must be a standard method?
		other_hand.add(card)				


# Inheriting from Base Class
class Deck(Hand):							# inherit:   class new_class(base_class):
	""" A Deck of Cards """
	def populate(self):
		for suit in Card.SUITS:
			for rank in Card.RANKS:
				self.add(Card(rank, suit))

	def shuffle(self):
		import random
		random.shuffle(self.cards)

	def deal(self, hands, per_hand=1):
		for rounds in range(per_hand):
			for hand in hands:
				if self.cards:
					top_card = self.cards[0]
					self.give(top_card, hand)
				else:
					print "Out of cards"




# main

deck = Deck();		print deck
deck.populate();	print deck
deck.shuffle();		print deck


hand1 = Hand(); 	
hand2 = Hand();
print "hand1:", hand1, "\t\thand2:", hand2


hands = [hand1, hand2];		#print hands

deck.deal(hands, per_hand=5)
print "hand1:", hand1, "\t\thand2:", hand2
# create cards objects
# card1 = Card(rank="A", suit="c")
# card2 = Card(rank="2", suit="c")
# card3 = Card(rank="3", suit="c")
# card4 = Card(rank="4", suit="c")
# card5 = Card(rank="5", suit="c")
# print card1, card2, card3, card4, card5


# # create Hand objects
# hand1 = Hand()
# hand2 = Hand()
# print "hand1:", hand1, "\t\thand2:", hand2

# # add()
# hand1.add(card1)
# hand1.add(card2)
# hand1.add(card3)
# hand1.add(card4)
# hand1.add(card5)
# print "hand1:", hand1, "\t\thand2:", hand2

# # give()  - interacting with different objects
# hand1.give(card5, hand2)		
# print "hand1:", hand1, "\t\thand2:", hand2
# hand1.give(card4, hand2)		
# print "hand1:", hand1, "\t\thand2:", hand2

# # clear()
# hand1.clear()
# print "hand1:", hand1, "\t\thand2:", hand2


