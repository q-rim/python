#! /usr/bin/python

# NOTE 
# Depending on the TYPE you use on for-loops, it will iterate through that type.
# EX:
#if you use for x in <TYPE> where TYPE is a string, then x will iterate through each char of the stirng.
#if you use for x in <TYPE> where TYPE is a int range(1,100), then x will iterate through each int of the range()
#if you use for x in <TYPE> where TYPE is a list or Tuple, then x will iterate through each int of the elements in the int or tuple.

# For-loop example

# Program:  Print each letter of the word in a single line
print("--- Print each letter of the word in a single line ---")
# this script takes your input and prints one letter on each line.

userIN = raw_input("Enter a word or a phrase: ");

for i in userIN:		# interesting how the i turns into the object.
	print(i);

print("--- Counter: for i in \"testing\"");
for i in "testing":
	print(i);

print("--- Counter: for i in [1,2,3,4,5,6]");
for i in [1,2,3,4,5,6]:
	print(i);

# Program:  Counter
print "range(3,10) =";
print range(3,10);
print("--- Counter: for i in range(10):");
for i in range(10):
	print(i);

print("--- Counter: for i in range(3, 10):");
for i in range(3, 10):
	print(i);

print("--- Counter: for i in range(3,26,5):");
for i in range(3,26,5):
	print(i);

print("--- Counter: for i in range(10,3,-1):");
for i in range(10,3,-1):
	print(i);
