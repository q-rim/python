#! /usr/bin/python

# Dictionaries {key1:def1, key2:def2}
# Geek Translator Program:

# allows users to:
# 1. Lookup terms
# 2. Add a terms
# 3. Replace definition
# 4. Delete a term

def keyExist(key, dic):
	"""this is my function documentation:  doc-string - will show up on IDE"""
	if key in dic:
		return True;
	else:
		return False;


print\
""" 
	Geek Translator

	0.  Quit
	1.  Lookup a term
	2.  Add a term
	3.  Replace a term
	4.  Delete a term
"""

geek = {"200" : "OK/Success",
		#"301" : "Permanent",
		#"302" : "Temporary",
		"404" : "Not Found",
		"500" : "Server Error",
		"503" : "Unavailable",
}

exit = False;
while exit == False:
	print 
	uIN = raw_input("# Enter your selection: ");
	# Looking up a Term
	if uIN == "1":
		print "1. Lookup a term.  Here are the list of keys:"; 
		print geek.keys();
		keyIN = raw_input("Which term would you like to look up? ");

		while keyExist(key=keyIN, dic=geek) == False:		# check to see if key exists
			keyIN = raw_input("Not in dictionary.  Enter a new KEY: ");
		
		print keyIN, ":", geek[keyIN]			# look up value
		print keyIN, ":", geek.get(keyIN)		# look up value
	
	# Add a Term
	elif uIN == "2":
		print "2. Add a term."

		keyIN = raw_input("Enter key: ");
		while keyExist(key=keyIN, dic=geek) == True:		# check to see if key exists
			keyIN = raw_input("Already in dictionary.  Enter a new KEY: ");
	
		valIN = raw_input("Enter the val: ");
		geek0 = dict(geek);						# create a new dictionary.  Dictionaries are objects that point to same mem-location.
		geek[keyIN]=valIN;						# input the key:val item.
		print keyIN, ":", geek[keyIN], "-entered into the Dictionary"
		print "BEFORE = ", geek0;
		print "AFTER = ", geek;

	# Replace a Term
	elif uIN == "3":
		print "3. Replace a term. List of items available to replace"
		print geek
		
		keyIN = raw_input("Select key to the item you would like to replace: ");
		while keyExist(key=keyIN, dic=geek) == False:		# check to see if key exists
			keyIN = raw_input("Not in dictionary.  Enter a new KEY: ");
			
		valIN = raw_input("Enter the new value (definition): ");
		geek0 = dict(geek);						# create a new dictionary.  Dictionaries are objects that point to same mem-location.
		geek[keyIN]=valIN;						# input the key:val item.
		print "BEFORE = ", geek0;
		print "AFTER = ", geek;

	# Delete a term
	elif uIN == "4":
		print "4. Delete a term.  List of items available to replace."
		print geek;
		
		keyIN = raw_input("Select key to the item you would like to delete: ");
		while keyExist(key=keyIN, dic=geek) == False:		# check to see if key exists
			keyIN = raw_input("Not in dictionary.  Enter a new KEY: ");
		
		geek0 = dict(geek);						# create a new dictionary.  Dictionaries are objects that point to same mem-location.
		del geek[keyIN];						# delete item
		print "BEFORE = ", geek0;
		print "AFTER = ", geek;

	else:
		print "exiting. Good Day! :)"
		exit = True;

