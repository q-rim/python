#! /usr/bin/python

# Simple Critter
# Attribute = Variables.

class Critter(object):						# base my class on "object"
	"""A Virtual Pet"""
	
	total = 0;								# Class Attribute.  Class Variable

	def __init__(self, name):				# Constructor = Initialization Method() - automatically runs each time a critter object is created.  It "Constructs" a critter object.
		Critter.total +=1;					# Class Attribute.  Class Variable - Accessing/Using it.
		print "A new critter has been born!  Total number of critters: "+str(Critter.total);
		self.name = name;
		

	def __str__(self):						# str_method Allows you to print your Object.  Printing without this method will print the Object Memory location.
		rep = "Critter object name: " + self.name + "\n"
		return rep


	def talk(self):							# Class method
		print "Critter#"+str(Critter.total)+":  Hi. I'm ", self.name, "\n"


# main
crit1 = Critter("1-Poochie")				# Instantiate(Create) and initialize a Critter Object.
print crit1									# call __str__() method
crit2 = Critter("2-Booger")					# Instantiate(Create) and initialize a Critter Object.
print crit2									# call __str__() method

crit2.talk()								# call crit()'s talk method.

# Direct Access
# Usually, you want to avoid directly accessing the Object variable.  Use get/set methods instead.
print "crit.name: ", crit2.name 									# Directly get Object Attribute.  Access Object value.
crit2.name = "Loogie"; 		print "crit.name: ", crit2.name 		# Directly set object variable.

print "Total number of critters =", Critter.total 					# Directly get Class Attribute. 
#Critter.Total = "Snot"											    # Directly set Class Attribute.  <-- you cannot do this.